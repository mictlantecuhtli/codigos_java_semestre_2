/*
* Autor: Fátima Azucena MC
* Fecha: 22_03_23
* Correo: fatimaazucenamartinez274@gmail.com
*/

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JOptionPane;


public class InicioSesión extends JFrame {

    private JLabel usernameLabel;
    private JLabel passwordLabel;
    private JTextField usernameField;
    private JTextField passwordField;
    private JButton loginButton;
    private JButton closeButton;
    private JButton cancelButton;
    private byte intentos = 0;

    public InicioSesión() {
        // Establecer el título y tamaño de la ventana
        Eventos manejador = new Eventos();
        setSize(400, 300);

        // Crear los componentes que se agregarán a la ventana
        usernameLabel = new JLabel("Usuario:");
        passwordLabel = new JLabel("Contraseña:");
        usernameField = new JTextField(10);
        passwordField = new JTextField(10);
        loginButton = new JButton("Login");
        cancelButton = new JButton("Clean");
        closeButton = new JButton("Close");

        // Establecer el LayoutManager de la ventana
        setLayout(new GridLayout(3, 3));

        // Agregar los componentes a la ventana
        add(usernameLabel);
        add(usernameField);
        add(loginButton);
        add(passwordLabel);
        add(passwordField);
        add(cancelButton);
        add(closeButton);

        // Asignar el evento a los botones
        //loginButton.addActionListener(manejador);
        //cancelButton.addActionListener(manejador);
        //closeButton.addActionListener(manejador);

        // Hacer visible la ventana
        setVisible(true);
    }

    public static void main(String[] args) {
        InicioSesión ventana = new InicioSesión();
    }
    public void logear () {
              String us = usernameField.getText();
              String pass = passwordField.getText();
              String usuarios[][] = {{"Fatima","kiiroi17"},
                                     {"Mary","pixiemania"},
                                     {"Raul","berserker"}
                                    };
              byte i;
              byte y;
              boolean retorno = false;

                  for ( i = 0; i < 3; i++ ) {
                      retorno = ( ( usuarios[i][0].equals(us) ) && ( usuarios[i][1].equals(pass) ) );
                      if ( retorno ){
                          break;
                      }
                  }

                  if ( retorno ) {
                      JOptionPane.showMessageDialog(null,"Usuario y contraseña correctas");
                      JOptionPane.showMessageDialog(null,"Bienvenido");
                  } else if ( intentos == 3 ){
                        System.exit(0);
                  }
                  else {
                      JOptionPane.showMessageDialog(null,"Usuario o contraseña incorrectos");
                      JOptionPane.showMessageDialog(null,"Intento " + intentos + " de 3");
                      intentos++;
                  }

      }
      public void cancel () {
          usernameField.setText(null);
          passwordField.setText(null);
      }

      public void salida () {
          System.exit(0);
      }

      //class Eventos implements ActionListener {

          //@Override
          //public void actionPerformed ( ActionEvent ev ) {

              //if ( ev.getSource() == loginButton ) {
                  //logear();
              //}
              //if ( ev.getSource() == cancelButton ) {
                  //cancel();
              //}
              //if ( ev.getSource() == closeButton ) {
                  //salida();
              //}

          //}

      //}

}
