/*
* Autor: Fátima Azucena MC
* Fecha: 19_04_23
* Correo: fatimaazucenamartinez274@gmail.com
*/

// Paquetes
import javax.swing.JOptionPane;
import java.util.Scanner;


public class ProyectoHerencia {
    
    // Declaración de atributos
    private String nombre;
    public byte edad;
    protected String genero;
    byte numero;

    public ProyectoHerencia( String nombre , byte edad , String genero , byte numero) {
        this.nombre = nombre;
        this.edad = edad;
        this.genero = genero;
        this.numero = numero;	
    }

    private void name () {
        JOptionPane.showMessageDialog(null,"Tu nombre es: " + nombre);
    }

     public void age () {
        JOptionPane.showMessageDialog(null,"Tu edad es: " + edad);
    }

    protected void gender () {
        JOptionPane.showMessageDialog("Tu genero es: " + genero);    
    }

    byte number () {
      JOptionPane.showMessageDialog(null,"Tu número es: " + numero );  
    }

}

//class Herencia1 extends ProyectoHerencia {
    
   // public String nombres (String nombre ) {
        
	// Declaración de atributos
        //Scanner entrada = new Scanner(System.in);
        //String name;
    

        //System.out.print("Ingresa tu nombre: ");
        //name = entrada.nextLine();
        //imprimir(name);
    //}
//}

//class Herencia2 extends ProyectoHerencia {


//}

class Vecina extends ProyectoHerencia {

    public static void main ( String[] args ) {
        
        // Declaración de variables
	Scanner entrada = new Scanner (System.in);
	String name;
	ProyectoHerencia obj = new ProyectoHerencia();

	obj.age();

    }

}
