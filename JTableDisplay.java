import java.awt.Color;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class JTableDisplay extends JFrame{
    
    JPanel panel;
    

    public JTableDisplay() {
        setSize(500,500);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(0);
        setLayout(null);
        panel = new JPanel();
        panel.setBounds(100, 100, 200, 50);
        panel.setBackground(Color.BLUE);
        add(panel);  
    }

    public static void main(String[] args) {
        JTableDisplay jt=new JTableDisplay();
        jt.setVisible(true);
    }
}
