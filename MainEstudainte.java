
import java.util.Scanner;

class Estudiante {
    private String nombres;
    private String apellidos;
    private String carnet;
    private String carrera;
    private int edad;
    
    Estudiante ( String nombres, String Apellidos, String carnet, String carrera, int edad){
        setNombres(nombres);
        setApellidos(apellidos);
        setCarnet(carnet);
        setCarrera(carrera);
        setEdad(edad);
    }
    
    //Métodos modificadores
    public void setNombres ( String n ) {
        nombres = n;
    }
    public void setApellidos ( String a ) {
        apellidos = a;
    }
    public void setCarnet ( String c ) {
        carnet = c;
    }
    public void setCarrera ( String cr ) {
        carrera = cr;
    }
    public void setEdad ( int e ) {
        edad = e;
    }
    
    //Métodos accesores
    public String getNombres () {
        return nombres;
    }
    public String getApellidos () {
        return apellidos;
    }
    public String getCarnet () {
        return carnet;
    }
    public String getCarrera () {
        return carrera;
    }
    public int getEdad () {
        return edad;
    } 
    public void imprimirEstudiantes () {
        System.out.print("\nNombres: " + getNombres() + "\nApellidos: " + getApellidos() + "\nCarnet: " + getCarnet() + "\nCarrera: " + getCarrera() + "\nEdad: " + getEdad());
    }
}

public class MainEstudiante {
    static Scanner entrada = new Scanner(System.in);
    
    public static void main ( String[] args ) {
        String nombres;
        String apellidos;
        String carnet;
        String carrera;
        int edad;
        
        System.out.print("Favor ingresar nombres: ");
        nombres = entrada.nextLine();
        System.out.print("Favor ingresar apellidos: ");
        apellidos = entrada.nextLine();
        System.out.print("Favor ingresar el número del carnet: ");
        carnet = entrada.nextLine();
        System.out.print("Favor ingresar nombre de la carrera: ");
        carrera = entrada.nextLine();
        System.out.print("Favor ingresar la edad: ");
        edad = entrada.nextInt();
        
        Estudiante e;
        e = new Estudiante( nombres,apellidos,carnet,carrera,edad);
        e.imprimirEstudiantes();
        
    }
}

