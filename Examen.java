/*
 *
 *
 *
*/

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.Container;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JLabel;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class Examen extends JFrame {

	// Declarar variables
	private JButton aceptar;
	private JButton cancelar;
	private JButton salir;
	private JPanel panel;
	private JPanel panel2;
	private JLabel pass;
	private JLabel user;
	private JTextField passField;
	private JTextField userField;

	public Examen () {
	
            // Declarar objetos
	    Container contenedor = getContentPane();
	    Container contenedor2 = getContentPane();
	    contenedor.setLayout(new BorderLayout());
	    contenedor2.setLayout(new BorderLayout());

	    panel = new JPanel(new GridLayout(2,2));

	    user = new JLabel("User ");
	    userField = new JTextField ( );
	    pass = new JLabel ("Password ");
	    passField = new JTextField ( );

	    panel.add(user);
	    panel.add(userField);
	    panel.add(pass);
	    panel.add(passField);

	    contenedor.add(panel,BorderLayout.CENTER);

	    panel2 = new JPanel(new GridLayout(1,3));

            aceptar = new JButton ("Acept");
	    cancelar = new JButton ("Cancel");
	    salir = new JButton ("Close");

	    panel2.add(aceptar);
	    panel2.add(cancelar);
	    panel2.add(salir);

	    contenedor2.add(panel2,BorderLayout.SOUTH);

	    setVisible(true);
	    setLocationRelativeTo(null);
	    setSize(500,500);
	 
	}

	public static void main ( String[] args ) {
		
		Examen ventana = new Examen();
		ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	}


        class Eventos implements ActionListener {
	    
		@Override
		public void actionPerformed ( ActionEvent ev ){
		
			if (ev.getSource() == aceptar ){
			     JOptionPane.showMessageDialog(null,"Aceptar");
			}
			if ( ev.getSource() == cancelar ) {
			    JOptionPane.showMessageDialog(null,"Cancelar");
			}
			if ( ev.getSource() == salir ) {
			    JOptionPane.showMessageDialog(null,"Salir");
			}
		
		}
	}	
}
