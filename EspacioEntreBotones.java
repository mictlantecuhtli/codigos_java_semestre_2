import java.awt.FlowLayout;
import javax.swing.JButton;
import javax.swing.JFrame;

public class EspacioEntreBotones extends JFrame {
    
    public EspacioEntreBotones() {
        super("Ejemplo de espaciado entre botones");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        setLayout(new FlowLayout(FlowLayout.CENTER, 50, 10)); // Definimos un FlowLayout con una separación horizontal de 50 píxeles y una separación vertical de 10 píxeles
        
        add(new JButton("Botón 1"));
        add(new JButton("Botón 2"));
        add(new JButton("Botón 3"));
        
        pack();
        setVisible(true);
    }
    
    public static void main(String[] args) {
        new EspacioEntreBotones();
    }
}
