import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JButton;

public class MyFrame2 extends JFrame { 

    private JButton siguiente;

    public MyFrame2() {
        // Crear un panel y asignarle un GridBagLayout
        JPanel panel = new JPanel(new GridBagLayout());

        // Crear los componentes que queremos agregar
        JLabel semestre = new JLabel("Semestre: ");
        String[] semestreOpciones = {"1", "2", "3","4","5","6","7","8","9"};
        JComboBox<String> comboBoxSemestre = new JComboBox<>(semestreOpciones);

        JLabel periodo = new JLabel("Perido:");
        String[] periodoOpciones = {"2023-1", "2023-2", "2023-3","2023-4","2023-5","2023-6","2023-7","2023-8","2023-9"};
        JComboBox<String> comboBoxPeriodo = new JComboBox<>(periodoOpciones);

        JLabel asignatura = new JLabel("Asignatura: ");
        String[] asignaturaOpciones = {"C.Integral", "POO", "Probabilidad y Estádistica","Administracón","Física","Algebra Lineal"};
        JComboBox<String> comboBoxAsignatura = new JComboBox<>(asignaturaOpciones);

	JButton siguiente = new JButton("Siguiente");

        // Crear un objeto GridBagConstraints para especificar las restricciones de cuadrícula
        GridBagConstraints c = new GridBagConstraints();

        // Agregar los componentes al panel utilizando las restricciones apropiadas
        c.gridx = 0;
        c.gridy = 0;
        panel.add(semestre, c);

        c.gridx = 1;
        c.gridy = 0;
        panel.add(comboBoxSemestre, c);

        c.gridx = 0;
        c.gridy = 1;
        panel.add(periodo, c);

        c.gridx = 1;
        c.gridy = 1;
        panel.add(comboBoxPeriodo, c);

        c.gridx = 0;
        c.gridy = 2;
        panel.add(asignatura, c);

        c.gridx = 1;
        c.gridy = 2;
        panel.add(comboBoxAsignatura, c);

	c.gridx = 0;
	c.gridy = 4;
	panel.add(siguiente,c);

        // Agregar el panel al JFrame
        this.add(panel);

        // Configurar la ventana
        this.pack();
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
    }

    public static void main(String[] args) {
        new MyFrame2();
    }
}
