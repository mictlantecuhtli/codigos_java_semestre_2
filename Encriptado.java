// Autor: Fátima Azucena MC
// Correo: fatimaazucenamartinez274@gmail.com
// Fecha: 09_03_2023

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Encriptado extends JFrame { // Inicio clase Encriptado
    
    // Declaración de atributos
    private JTextField entrada;
    //private JLabel entradaLbl;
    private JLabel salida;
    private JLabel salidaCadena;
    private JButton encriptar;
    private JButton desencriptar;
    private JPanel central;
    private JButton salir;
    private JButton borrar;
    
    public Encriptado() { // Inicio constructor Encriptado
    
        // Declaración de objetos
	Eventos manejador = new Eventos();
        Container contenedor = getContentPane();
        contenedor.setLayout(new BorderLayout());
        central = new JPanel(new GridLayout(2,2));
        entrada = new JTextField(10);
        //entradaLbl = new JLabel("Texto a encriptar");
        salida = new JLabel("Encriptar");
        salidaCadena = new JLabel("Descencriptar");
        encriptar = new JButton ("Encriptar");
	encriptar.addActionListener(manejador);
        desencriptar = new JButton ("Descencriptar");
	salir = new JButton("Salir");
	salir.addActionListener(manejador);
	borrar = new JButton("Borrar");
	borrar.addActionListener(manejador);
	desencriptar.addActionListener(manejador);
	central = new JPanel ( new GridLayout(0,1));
        //central.add(entradaLbl);
        central.add(entrada);
        central.add(encriptar);
        central.add(salida);
        central.add(desencriptar);
        central.add(salidaCadena);
	central.add(salir);
	central.add(borrar);
        contenedor.add(central,BorderLayout.CENTER);
        setSize(300,300);
        setVisible(true);
        setTitle("Encriptar-descencriptar");
        
    } // Fin constructor Encriptado

    public String action ( boolean valor , char array[] ) { // Inicio método action
	
        // Declaración de variables
	String retorno;
	int l;
	int i;
        l = array.length;
        
	if ( valor == true ) { // Inicio if
            
            for ( i = 0; i < l; i++ ) { // Inicio for
					//
                array[i] = ( char )( array[i] + (char) l );

            } // Fin for 1

            retorno = String.valueOf(array);

        } // Fin if
	
	else { // Inicio else

            for ( i = 0; i < l; i++ ) { // Inicio for
               
                array[i] = ( char )( array[i] - (char) l );

            } // Fin for 1

	    retorno = String.valueOf(array);

	}

	return retorno;

    } // Fin método action
    
    public void salida () { // Inicio método salir
    
        System.exit(0);
 	 
    } // Fin método salir 
    
    public void borrador () { // Inicio método borrar 
    
        entrada.setText(null);
        salida.setText(null);
        salidaCadena.setText(null);
    
    } // Fin método borrar

    public static void main ( String[] args ) { // Inicio método principal
    
        // Creación de objetos
	Encriptado ventana = new Encriptado();
	ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

   } // Fin método principal
    
    class Eventos implements ActionListener { // Inicio clase Eventos 
    
        @Override
        public void actionPerformed ( ActionEvent ev ) { // Inicio actionPerformed    
	    
            if ( ev.getSource() == encriptar ) { // Inicio if		
	    
               //Llamada a método action
		salida.setText(action(true, entrada.getText().toCharArray()));
	    
	    } // Fin if
	    if ( ev.getSource() == desencriptar ) { // Inicio if
                
		// Llamada a método action
                salidaCadena.setText(action(false, salida.getText().toCharArray()));

            } // Fin if
	    if ( ev.getSource() == salir ) { // Inicio if
			
                // Llamada método salir
		salida();

	    } // Fin if
	    if ( ev.getSource() == borrar ) {
		
                // Llamada a método borrador
		borrador();

	    } // Fin if
        } // Fin método actionPerformed
    
    } // Fin clase Enventos
    
} // Fin clase encriptado

