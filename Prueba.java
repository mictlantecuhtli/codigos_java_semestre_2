import javax.swing.*;

public class Prueba extends JFrame {

    public Prueba() {
        setTitle("Ejemplo");
        setSize(300, 200);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        
        // Creamos las etiquetas y los combobox
        JLabel etiqueta1 = new JLabel("Opción 1:");
        JComboBox<String> combo1 = new JComboBox<>(new String[] {"Valor 1", "Valor 2", "Valor 3"});
        
        JLabel etiqueta2 = new JLabel("Opción 2:");
        JComboBox<String> combo2 = new JComboBox<>(new String[] {"Valor 4", "Valor 5", "Valor 6"});
        
        JLabel etiqueta3 = new JLabel("Opción 3:");
        JComboBox<String> combo3 = new JComboBox<>(new String[] {"Valor 7", "Valor 8", "Valor 9"});
        
        // Creamos el botón
        JButton boton = new JButton("Aceptar");
        
        // Creamos el panel y añadimos los componentes
        JPanel panel = new JPanel();
        panel.add(etiqueta1);
        panel.add(combo1);
        panel.add(etiqueta2);
        panel.add(combo2);
        panel.add(etiqueta3);
        panel.add(combo3);
        panel.add(boton);
        
        // Añadimos el panel a la ventana
        getContentPane().add(panel);
        
        setVisible(true);
    }
    
    public static void main(String[] args) {
        new Prueba();
    }
}
