import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class MyFrame extends JFrame {

    public MyFrame() {
        // Crear un panel y asignarle un GridBagLayout
        JPanel panel = new JPanel(new GridBagLayout());

        // Crear los componentes que queremos agregar
        JLabel label1 = new JLabel("Texto 1:");
        JTextField textField1 = new JTextField(20);

        JLabel label2 = new JLabel("Texto 2:");
        JTextField textField2 = new JTextField(20);

        JButton boton1 = new JButton("Botón 1");
        JButton boton2 = new JButton("Botón 2");

        // Crear un objeto GridBagConstraints para especificar las restricciones de cuadrícula
        GridBagConstraints c = new GridBagConstraints();

        // Agregar los componentes al panel utilizando las restricciones apropiadas
        c.gridx = 0;
        c.gridy = 0;
        panel.add(label1, c);

        c.gridx = 1;
        c.gridy = 0;
        panel.add(textField1, c);

        c.gridx = 0;
        c.gridy = 1;
        panel.add(label2, c);

        c.gridx = 1;
        c.gridy = 1;
        panel.add(textField2, c);

        c.gridx = 0;
        c.gridy = 2;
        panel.add(boton1, c);

        c.gridx = 1;
        c.gridy = 2;
        panel.add(boton2, c);

        // Agregar el panel al JFrame
        this.add(panel);

        // Configurar la ventana
        this.pack();
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
    }

    public static void main(String[] args) {
        new MyFrame();
    }
}
