import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class RegistroCalificacion extends JFrame implements ActionListener {
    private JLabel labelTitulo;
    private JLabel labelNombre;
    private JLabel labelMateria;
    private JLabel labelCalificacion;

    private JTextField textFieldNombre;

    private JComboBox<String> comboBoxMateria;

    private JSpinner spinnerCalificacion;

    private JButton botonRegistrar;

    public RegistroCalificacion() {
        super("Registro de Calificaciones");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(400, 200);
        setLayout(new GridLayout(4, 2));

        labelTitulo = new JLabel("Registro de Calificaciones", JLabel.CENTER);
        labelTitulo.setFont(new Font("Arial", Font.BOLD, 16));
        add(labelTitulo);
        add(new JLabel(""));

        labelNombre = new JLabel("Nombre del Alumno:");
        add(labelNombre);
        textFieldNombre = new JTextField();
        add(textFieldNombre);

        labelMateria = new JLabel("Materia:");
        add(labelMateria);
        String[] materias = {"C.Integral", "POO", "Probabilidad y Estádistica", "Administración","Algebra Lineal"};
        comboBoxMateria = new JComboBox<>(materias);
        add(comboBoxMateria);

        labelCalificacion = new JLabel("Calificación:");
        add(labelCalificacion);
        spinnerCalificacion = new JSpinner(new SpinnerNumberModel(0, 0, 100, 1));
        add(spinnerCalificacion);

        botonRegistrar = new JButton("Registrar Calificación");
        botonRegistrar.addActionListener(this);
        add(new JLabel(""));
        add(botonRegistrar);

        setVisible(true);
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == botonRegistrar) {
            String nombre = textFieldNombre.getText();
            String materia = comboBoxMateria.getSelectedItem().toString();
            int calificacion = (int) spinnerCalificacion.getValue();

            // Procesa la calificación registrada
            System.out.println("Calificación registrada para " + nombre + " en " + materia + ": " + calificacion);
        }
    }

    public static void main(String[] args) {
        new RegistroCalificacion();
    }
}

