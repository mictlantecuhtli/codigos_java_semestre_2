import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class Interfaz extends JFrame {
    
    private JLabel etiqueta;
    private JTextField cajaTexto;
    private JButton boton;

    public Interfaz() {
        super("Interfaz de prueba");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        etiqueta = new JLabel("Introduce un texto:");
        cajaTexto = new JTextField(20);
        boton = new JButton("Mostrar");
        
        setLayout(new FlowLayout());
        add(etiqueta);
        add(cajaTexto);
        add(boton);
        
        boton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                System.out.println(cajaTexto.getText());
            }
        });
        
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }

    public static void main(String[] args) {
        Interfaz interfaz = new Interfaz();
    }
}
