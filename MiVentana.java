import javax.swing.*;
import java.awt.*;

public class MiVentana extends JFrame {
    
    public MiVentana() {
        // Establecer el título y tamaño de la ventana
        setTitle("Mi ventana");
        setSize(400, 300);
        
        // Crear los componentes que se agregarán a la ventana
        JLabel etiqueta = new JLabel("Etiqueta");
        JButton boton = new JButton("Botón");
        
        // Establecer el LayoutManager de la ventana
        setLayout(new BorderLayout());
        
        // Agregar los componentes a la ventana en las posiciones apropiadas
        add(etiqueta, BorderLayout.NORTH);
        add(boton, BorderLayout.CENTER);
        
        // Hacer visible la ventana
        setVisible(true);
    }
    
    public static void main(String[] args) {
        MiVentana ventana = new MiVentana();
    }
}
