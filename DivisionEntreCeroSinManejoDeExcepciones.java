// Autor: Fátima Azucena MC
// Fecha: 21_03_23
// Correo: fatimaazucenamartinez274@gmail.com

import java.util.Scanner;

public class DivisionEntreCeroSinManejoDeExcepciones {
    
    public static int cociente(int numerador, int denominador) {
        return numerador / denominador; // posible división entre cero
    }

    public static void main(String[] args) {
        Scanner explorador = new Scanner(System.in);
        System.out.print("Introduzca un numerador entero: ");
        int numerador = explorador.nextInt();
        System.out.print("Introduzca un denominador entrero: ");
        int denominador = explorador.nextInt();
        int resultado = cociente(numerador, denominador);
        System.out.printf("\nNumerador: " + numerador + "\nDenominador: " + denominador + "\nResulado: " + resultado);
	System.out.printf("\n");
    }

}
