/*
* Autor: Fátima Azucena MC
* Fecha: 23_03_23
* Correo: fatimaazucenamartinez274@gmail.com
*/

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class ConnectionDB {

    // Declaración de la conexión a mysql
    public static Connection con;

    // Declaración de los datos de la conexión
    private static final String driver = "com.mysql.jdbc.Driver";
    private static final String us = "HoneyData";
    private static final String pwd = "fedoraDB";
    private static final String url = "jdbc:mysql://localhost:3306/usuariosDB?characterEnconding=utfs";

    // Función que establece conexión
    public Connection conectar () {
        con = null;
	try {
	    con = (Connection) DriverManager.getConnection(url,us,pwd);
	    if ( con != null ) {
	    	
	    }
	}
	catch (SQLException e) {
		JOptionPane.showMessageDialog(null,"Error" + e.toString());
	}
	return con;
    }	

}
