/*
* Autor: Fátima Azucena MC
* Fecha: 22_03_23
* Correo: fatimaazucenamartinez274@gmail.com
*/


//Librerias
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JPasswordField; 
public class Login extends JFrame { // Inicio clase Login

    // Declaración de atributos
    private JPanel panel;
    private JLabel label1;
    private JLabel label2;
    private JButton button1;
    private JButton button2;
    private JButton button3;
    private JTextField textField1;
    private JTextField textField2;
    private JPasswordField passwordField; 

    public Login () { // Inicio constructor Login

	//Declaracion de objetos
	Eventos manejador = new Eventos();
        panel = new JPanel();
        label1 = new JLabel("Usuario");
        label2 = new JLabel("Contraseña");
        textField1 = new JTextField();
        passwordField = new JPasswordField();
        textField2 = new JTextField();
        button1 = new JButton("Aceptar");
	button1.addActionListener(manejador);
        button2 = new JButton("Cancelar");
	button2.addActionListener(manejador);
        button3 = new JButton("Salir");
	button3.addActionListener(manejador);
	panel.setBackground(new java.awt.Color(0, 51, 51));
	label1.setText("Usuario");
	label2.setText("Contraseña");
	button1.setText("Aceptar");
	button2.setText("Cancelar");
	button3.setText("Salir");
	setVisible(true);


        // Diseño de interfaz
	javax.swing.GroupLayout panelLayout = new javax.swing.GroupLayout(panel);
	panel.setLayout(panelLayout);
	
	panelLayout.setHorizontalGroup (
		panelLayout.createParallelGroup (javax.swing.GroupLayout.Alignment.LEADING)
		.addGroup(panelLayout.createSequentialGroup()
                .addGap(39, 39, 39)
		.addGap(39, 39, 39)
                .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(label1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(label2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(27, 27, 27)
                .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(textField2, javax.swing.GroupLayout.DEFAULT_SIZE, 114, Short.MAX_VALUE)
                    .addComponent(passwordField))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(textField1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(panelLayout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addComponent(button1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 57, Short.MAX_VALUE)
                .addComponent(button2)
                .addGap(39, 39, 39)
                .addComponent(button3)
                .addGap(40, 40, 40))
        );
	panelLayout.setVerticalGroup(
            panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelLayout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(label1)
                    .addComponent(textField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(textField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(60, 60, 60)
                .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(label2)
                    .addComponent(passwordField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 105, Short.MAX_VALUE)
                .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(button1)
                    .addComponent(button2)
                    .addComponent(button3))
                .addGap(28, 28, 28))
        );
	javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    } // Fin constructor Login	 	

    public static void main ( String[] args ) { // Inicio método principal
        
        Login ventana = new Login();
        ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);	    
    } // Fin método principal

    public void logear () {
        Access con = new Access();
	con.accesoUsuario(textField2.getText() , passwordField.getText());
    }
    
    public void cancel () {
        textField2.setText(null); 
        passwordField.setText(null);	
    }

    public void salir () {
    	System.exit(0);
    }

    class Eventos implements ActionListener {
    
        @Override
	public void actionPerformed ( ActionEvent ev ) {
	
	    if ( ev.getSource() == button1 ) {
	        logear();
	    }
	    if ( ev.getSource() == button2 ) {
	    	cancel();
	    } 
	    if ( ev.getSource() == button3 ) {
	    	salir();
	    }
	
	} 
    
    }	

}

