/*
* Autor: Fátima Azucena MC
* Fecha: 23_03_23
* Correo: fatimaazucenamartinez274@gmail.com
*/

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.swing.JOptionPane;

public class Access {
    public void accesoUsuario ( String us , String pwd ) {

        ConnectionDB db = new ConnectionDB();
        String usuarioCorrecto = null;
        String passCorrecto = null;

        try {
            Connection cn = db.conectar();
    	    PreparedStatement pst = cn.prepareStatement("SELECT id, name , pass FROM usuarios");
	    ResultSet rs = pst.executeQuery();	

	    if (rs.next()){
	        usuarioCorrecto = rs.getString(1);
	        passCorrecto = rs.getString(2);
	    }

	    if (us.equals(usuarioCorrecto) && (pwd.equals(passCorrecto))) {
	        JOptionPane.showMessageDialog(null,"Login correcto, Bienvenido" + us);
	    }
	    else if (!us.equals(usuarioCorrecto) || (!pwd.equals(passCorrecto))) {
	        JOptionPane.showMessageDialog(null,"Usuario o contraseña incorrecta");
	    }
        }
        catch ( Exception e ) {
    	    JOptionPane.showMessageDialog(null,"Error" + e );
        }
    }
}
