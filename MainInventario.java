
import java.util.Scanner;

class Inventario {
    private String referencia;
    int cantidad;
    
    Inventario ( String referencia, int cantidad ){
        setReferencia(referencia);
        setCantidad(cantidad);
    }
    
    //Metodos modificadores
    public void setReferencia ( String c ) {
        referencia = c;
    }
    public void setCantidad ( int n ) {
        cantidad = n;
    }
    
    //Métodos accesores
    public String getReferencia (){
        return referencia;
    }
    public int getCantidad () {
        return cantidad;
    }
    public void mostrarInventario (){
        System.out.print("\nReferencia: " + getReferencia() + "\nCantidad: " + getCantidad());
    }
}

public class MainInventario {
    static Scanner entrada = new Scanner(System.in);
    
    public static void main ( String[] args ) {
        String referencia;
        int cantidad;
        
        System.out.print("Favor ingresar referencia del producto: ");
        referencia = entrada.nextLine();
        System.out.print("Favor ingresar cantidad: ");
        cantidad = entrada.nextInt();
        
        Inventario i = new Inventario(referencia,cantidad);
        i.mostrarInventario();
    }
}

