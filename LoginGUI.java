import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class LoginGUI extends JFrame implements ActionListener {
    private JTextField usernameField;
    private JPasswordField passwordField;
    private JButton loginButton;
    
    public LoginGUI() {
        super("Login");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(300, 150);
        setLayout(new GridLayout(3, 2));
        
        JLabel usernameLabel = new JLabel("Username:");
	username.setBounds(10,10,250,50);
        add(usernameLabel);
        usernameField = new JTextField();
        add(usernameField);
        
        JLabel passwordLabel = new JLabel("Password:");
        add(passwordLabel);
        passwordField = new JPasswordField();
        add(passwordField);
        
        loginButton = new JButton("Login");
        loginButton.addActionListener(this);
        add(new JLabel(""));
        add(loginButton);
        
        setVisible(true);
    }
    
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == loginButton) {
            String username = usernameField.getText();
            String password = new String(passwordField.getPassword());
            // Verificar el usuario y la contraseña
            if (username.equals("usuario") && password.equals("contraseña")) {
                JOptionPane.showMessageDialog(this, "¡Bienvenido " + username + "!");
            } else {
                JOptionPane.showMessageDialog(this, "Usuario o contraseña incorrectos.");
            }
        }
    }
    
    public static void main(String[] args) {
        new LoginGUI();
    }
}
