import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

class FrameEleccion extends JFrame{

public FrameEleccion(){

    setTitle("Eleccion");
    setBounds(500,500,400,400);

    add(PanelEleccion, BorderLayout.NORTH);
    PanelEleccion.setLayout(new GridLayout(0,1,1,10));

    PanelEleccion.add(boton1);
    PanelEleccion.add(boton2);
    PanelEleccion.add(boton3);
    PanelEleccion.add(boton4);

    EventoEleccion eventoBotones=new EventoEleccion(this);

    boton1.addActionListener(eventoBotones);
    boton2.addActionListener(eventoBotones);
    boton3.addActionListener(eventoBotones);
    boton4.addActionListener(eventoBotones);

    setVisible(true);
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

}

class EventoEleccion implements ActionListener{

    private FrameEleccion eleccion;

    public EventoEleccion(FrameEleccion eleccion) {

        this.eleccion = eleccion;

    }

    public void actionPerformed(ActionEvent e) {

        JButton source = (JButton)e.getSource();

        if (boton1 == source  ){

            FrameEntrada frameEntrada = new FrameEntrada();
            eleccion.dispose();

        }else if (boton2 == source ){

            FrameSalida frameSalida=new FrameSalida();
            eleccion.dispose();

        }else if (boton3 == source ){

            FrameAnadirDatos frameAnadirDatos=new FrameAnadirDatos();
            eleccion.dispose();

        }else if (boton4 == source){

            eleccion.dispose();

        }

    }


}

private JPanel PanelEleccion = new JPanel();
private JButton boton1 = new JButton("Entrada");
private JButton boton2 = new JButton("Salida");
private JButton boton3 = new JButton("Añadir datos");
private JButton boton4 = new JButton("Cerrar");

}
