import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class prueba extends JFrame implements ActionListener {
    private JComboBox<String> comboBox1;
    private JComboBox<String> comboBox2;
    
    public prueba() {
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(400, 150);
        setLayout(new GridLayout(2, 2));
        
        String[] options1 = {"Opción 1", "Opción 2", "Opción 3"};
        comboBox1 = new JComboBox<>(options1);
        comboBox1.addActionListener(this);
        add(new JLabel("ComboBox 1:"));
        add(comboBox1);
        
        String[] options2 = {"Opción A", "Opción B", "Opción C"};
        comboBox2 = new JComboBox<>(options2);
        comboBox2.addActionListener(this);
        add(new JLabel("ComboBox 2:"));
        add(comboBox2);
        
        setVisible(true);
    }
    
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == comboBox1) {
            String selectedOption = comboBox1.getSelectedItem().toString();
            // Procesa la opción seleccionada del ComboBox 1
            System.out.println("Opción seleccionada del ComboBox 1: " + selectedOption);
        } else if (e.getSource() == comboBox2) {
            String selectedOption = comboBox2.getSelectedItem().toString();
            // Procesa la opción seleccionada del ComboBox 2
            System.out.println("Opción seleccionada del ComboBox 2: " + selectedOption);
        }
    }
    
    public static void main(String[] args) {
        new prueba();
    }
}
