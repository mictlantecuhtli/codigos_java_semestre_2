import javax.swing.*;
import java.awt.event.*;
import java.swing.JButton;

public class Ventana extends JFrame {
    public Ventana() {
        // Define los componentes de la ventana aquí
        // ...
        
        setSize(400, 300); // Establece el tamaño de la ventana
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}

public class Boton extends JFrame {
    public Boton() {
        JButton boton = new JButton("Mostrar ventana");
        boton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Ventana ventana = new Ventana();
                ventana.setVisible(true);
            }
        });
        add(boton);
        
        setSize(200, 100); // Establece el tamaño de la ventana
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    
    public static void main(String[] args) {
        Boton boton = new Boton();
        boton.setVisible(true);
    }
}
