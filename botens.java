import javax.swing.*;
import java.awt.*;

public class botens extends JFrame {

    public botens() {
        super("Cambiar tamaño de botones");

        // Crea un botón y establece su tamaño usando setSize()
        JButton boton1 = new JButton("Botón 1");
        boton1.setSize(100, 50);

        // Crea otro botón y establece su tamaño usando setPreferredSize()
        JButton boton2 = new JButton("Botón 2");
        boton2.setPreferredSize(new Dimension(150, 75));

        // Agrega los botones al contenedor
        JPanel panel = new JPanel();
        panel.add(boton1);
        panel.add(boton2);
        add(panel);

        setSize(300, 200);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }

    public static void main(String[] args) {
        new botens();
    }
}
