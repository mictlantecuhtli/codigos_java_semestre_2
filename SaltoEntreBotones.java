import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class SaltoEntreBotones extends JFrame {
    
    public SaltoEntreBotones() {
        super("Ejemplo de saltos entre botones");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        JPanel panel = new JPanel(new GridLayout(3, 2, 10, 10)); // Definimos un GridLayout con 3 filas, 2 columnas y un espacio de 10 píxeles entre cada celda
        
        panel.add(new JButton("Botón 1"));
        panel.add(new JButton("Botón 2"));
        panel.add(new JPanel()); // Agregamos una celda vacía para crear el espacio
        panel.add(new JButton("Botón 3"));
        panel.add(new JButton("Botón 4"));
        panel.add(new JPanel()); // Agregamos otra celda vacía para crear el espacio
        
        add(panel);
        pack();
        setVisible(true);
    }
    
    public static void main(String[] args) {
        new SaltoEntreBotones();
    }
}
