import javax.swing.*;
import java.awt.*;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JOptionPane;

public class EjemploLayout extends JFrame {
    
	private JTextField usernameField;
        private JPanel panel;
        private JPasswordField passwordField;
        private JButton loginButton;
        private JButton cancelButton;
        private JButton closeButton;
        private int intentos = 0 ;

	public EjemploLayout() {
	Eventos manejador = new Eventos();
        setTitle("Login");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(300, 250);
        
        // Creamos el panel principal con GridBagLayout
        JPanel panel = new JPanel(new GridBagLayout());
        
        // Creamos los labels y los textFields
        JLabel usernameLabel = new JLabel("Usuario:");
        JTextField usernameField = new JTextField(10);
        JLabel passwordLabel = new JLabel("Contraseña:");
        JTextField passwordField = new JTextField(10);
        
        // Agregamos el label1 y el textField1 a la primera fila
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.insets = new Insets(5, 5, 5, 5);
        panel.add(usernameLabel, gbc);
        
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 0;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.weightx = 1.0;
        gbc.insets = new Insets(5, 5, 5, 5);
        panel.add(usernameField, gbc);
        
        // Agregamos el label2 y el textField2 a la segunda fila
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.insets = new Insets(5, 5, 5, 5);
        panel.add(passwordLabel, gbc);
        
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 1;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.weightx = 1.0;
        gbc.insets = new Insets(5, 5, 5, 5);
        panel.add(passwordField, gbc);
        
        // Agregamos los botones a la tercera fila
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.gridwidth = 2;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(5, 5, 5, 5);
	JButton loginButton = new JButton("Login"); 
        panel.add(loginButton, gbc);
        loginButton.addActionListener(manejador);

        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 3;
        gbc.gridwidth = 2;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(5, 5, 5, 5);
	JButton cancelButton = new JButton("Cancel");
        panel.add(closeButton, gbc);
	cancelButton.addActionListener(manejador);

        
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 4;
        gbc.gridwidth = 2;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(5, 5, 5, 5);
	JButton closeButton = new JButton("Close");
        panel.add(closeButton, gbc);
	closeButton.addActionListener(manejador);
        
        // Agregamos el panel al frame
        getContentPane().add(panel);
        
        setVisible(true);
    }
    
    public static void main(String[] args) {
        new EjemploLayout();
    }
    public void logear () {
              String us = usernameField.getText();
              String pass = passwordField.getText();
              String usuarios[][] = {{"Fatima","kiiroi17"},
                                     {"Mary","pixiemania"},
                                     {"Raul","berserker"}
                                    };
              byte i;
              byte y;
              boolean retorno = false;

                  for ( i = 0; i < 3; i++ ) {
                      retorno = ( ( usuarios[i][0].equals(us) ) && ( usuarios[i][1].equals(pass) ) );
                      if ( retorno ){
                          break;
                      }
                  }

                  if ( retorno ) {
                      JOptionPane.showMessageDialog(null,"Usuario y contraseña correctas");
                      JOptionPane.showMessageDialog(null,"Bienvenido");
                  } else if ( intentos == 3 ){
                        System.exit(0);
                  }
                  else {
                      JOptionPane.showMessageDialog(null,"Usuario o contraseña incorrectos");
                      JOptionPane.showMessageDialog(null,"Intento " + intentos + " de 3");
                      intentos++;
                  }

      }
      public void cancel () {
          usernameField.setText(null);
          passwordField.setText(null);
      }

      public void salida () {
          System.exit(0);
      }

      class Eventos implements ActionListener {

          @Override
          public void actionPerformed ( ActionEvent ev ) {

              if ( ev.getSource() == loginButton ) {
                  logear();
              }
              if ( ev.getSource() == cancelButton ) {
                  cancel();
              }
              if ( ev.getSource() == closeButton ) {
                  salida();
              }

          }

      }

}

