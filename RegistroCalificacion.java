import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class RegistroCalificacion extends JFrame {

    private JLabel labelTitulo;
    private JLabel labelNombre;
    private JLabel labelUnidad;
    private JComboBox<String> comboBoxUnidad;
    private JLabel labelMateria;
    private JLabel labelCalificacion;
    private JComboBox<String> comboBoxMateria;
    private JSpinner spinnerCalificacion;
    private JButton botonRegistrar;
    
    public RegistroCalificacion() {

        setSize(500, 500);
        setLayout(new GridLayout(4, 2));
	JPanel panel = new JPanel(new GridBagLayout());
        
        labelTitulo = new JLabel("Registro de Calificaciones", JLabel.CENTER);
        labelTitulo.setFont(new Font("Arial", Font.BOLD, 16));
        add(labelTitulo);
        add(new JLabel(""));
       
	labelUnidad = new JLabel("Unidad:");
        add(labelUnidad);
        String[] unidad = {"1", "2", "3", "4","5","6"};
        comboBoxUnidad = new JComboBox<>(unidad);
        add(comboBoxUnidad);

        labelMateria = new JLabel("Materia:");
        add(labelMateria);
        String[] materias = {"C.Integral", "POO", "Probabilidad y Estádistica", "Administración","Algebra Lineal"};
        comboBoxMateria = new JComboBox<>(materias);
        add(comboBoxMateria);
        
        labelCalificacion = new JLabel("Calificación:");
        add(labelCalificacion);
        spinnerCalificacion = new JSpinner(new SpinnerNumberModel(0, 0, 100, 1));
        add(spinnerCalificacion);
        
        botonRegistrar = new JButton("Registrar Calificación");
        add(new JLabel(""));
        add(botonRegistrar);

	GridBagConstraints c = new GridBagConstraints();

	c.gridx = 1;
        c.gridy = 0;
        panel.add(labelUnidad, c);

        c.gridx = 0;
        c.gridy = 1;
        panel.add(comboBoxUnidad,c);
        setVisible(true);
    }
    
    public static void main(String[] args) {
       RegistroCalificacion calificaciones =  new RegistroCalificacion();
       calificaciones.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
}
